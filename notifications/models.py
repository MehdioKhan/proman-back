from django.db import models
from base import models as base_models
from django.contrib.postgres.fields import JSONField
from account.models import User


class Notification(base_models.Model):
    user = models.ForeignKey(to='account.User',related_name='notifications',
                             on_delete=models.CASCADE)
    kwargs = JSONField()

    seen = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created_at','-updated_at')

    def __str__(self):
        return self.user.email
