from rest_framework.routers import SimpleRouter
from . import views

app_name = 'notification'

router = SimpleRouter()
router.register('notifications',views.NotificationViewSet)

urlpatterns = router.urls
