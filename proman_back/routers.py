from account.urls import urlpatterns as account_urls
from blog.urls import urlpatterns as blog_urls
from note.urls import urlpatterns as note_urls
from project.attachments.urls import urlpatterns as attachment_urls
from project.task.urls import urlpatterns as task_urls
from project.urls import urlpatterns as project_urls
from notifications.urls import urlpatterns as notif_urls

urlpatterns = account_urls+blog_urls+note_urls+\
              attachment_urls+task_urls+project_urls+\
              notif_urls
