from .views import BlogViewSet
from rest_framework.routers import SimpleRouter


router = SimpleRouter()

router.register('blog',BlogViewSet)

urlpatterns = router.urls
