from django.urls import path
from rest_framework.routers import SimpleRouter
from . import views


app_name = 'account'

router = SimpleRouter()

router.register('role',views.RoleViewSet)

urlpatterns = [
    path('login/',views.login),
    path('logout/',views.logout),
    path('signup/',views.UserSignUp.as_view()),
    path('users/',views.UsersList.as_view()),
] + router.urls