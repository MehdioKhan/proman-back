from rest_framework.routers import SimpleRouter
from .views import TaskViewSet,CommentViewSet

app_name = 'task'

router = SimpleRouter()
router.register('task',TaskViewSet)
router.register('comment',CommentViewSet)

urlpatterns = router.urls
