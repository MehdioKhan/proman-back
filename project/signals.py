from .models import Membership
from .task.models import Task,Comment
from django.db.models.signals import post_save
from django.dispatch import receiver
from .tasks import send_user_notifications
from notifications.models import Notification


@receiver(post_save,sender=Membership)
def new_membership(sender,instance=None,**kwargs):
    update_diff = (instance.updated_at-instance.created_at).total_seconds()
    user = instance.user

    if update_diff < 1:
        notif = Notification.objects.create(
            user=user,
            kwargs={'new_membership': instance.id})
        send_user_notifications(notif)


@receiver(post_save,sender=Task)
def new_task(sender,instance=None,**kwrags):
    update_diff = (instance.updated_at-instance.created_at).total_seconds()
    user = instance.assigned_to

    if update_diff < 1:
        notif = Notification.objects.create(
            user=user,
            kwargs={'new_task': instance.id})
        send_user_notifications(notif)


@receiver(post_save,sender=Comment)
def new_comment(sender,instance=None,**kwrags):
    update_diff = (instance.updated_at-instance.created_at).total_seconds()

    if update_diff < 1:
        if instance.user is not instance.task.assigned_to:
            notif = Notification.objects.create(
                user=instance.task.assigned_to,
                kwargs={'new_comment': instance.id})
            send_user_notifications(notif)
