from .views import AttachmentsViewSet
from rest_framework.routers import SimpleRouter


router = SimpleRouter()

router.register('attachment',AttachmentsViewSet)

urlpatterns = router.urls
