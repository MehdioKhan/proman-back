from celery import shared_task
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from notifications.serializers import NotificationSerializer
import json

channel_layer = get_channel_layer()


@shared_task
def send_user_notifications(notif):
    data = NotificationSerializer(notif).data
    async_to_sync(channel_layer.group_send)(
        f"{notif.user.id}_notification",
        {
            "type": "notifications.notification",
            "data": json.dumps(data)
        }
    )
