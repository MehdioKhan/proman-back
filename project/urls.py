from django.urls import path,include
from rest_framework.routers import SimpleRouter
from . import views

app_name = 'project'

router = SimpleRouter()
router.register('project',views.ProjectViewSet,basename='projects')
router.register('task_status',views.TaskStatusViewSet)
router.register('membership',views.MembershipViewSet)


urlpatterns = [
    path('index',views.index,name='index'),
] + router.urls