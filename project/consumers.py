from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from project.models import Project
from django.db.models import Q


class NotificationConsumer(WebsocketConsumer):
    def connect(self):
        user = self.scope['user']
        if user.is_authenticated:
            # user_projects = Project.objects.filter(
            #     Q(owner=user)|Q(memberships__user=user)
            # )
            # for project in user_projects:
            #     async_to_sync(self.channel_layer.group_add)(f"{project.id}_notification", self.channel_name)
            async_to_sync(self.channel_layer.group_add)(f"{user.id}_notification", self.channel_name)
            self.accept()
        else:
            self.close()

    def disconnect(self, close_code):
        user = self.scope['user']
        if user.is_authenticated:
            # user_projects = Project.objects.filter(
            #     Q(owner=user) | Q(memberships__user=user)
            # )
            # for project in user_projects:
            #     async_to_sync(self.channel_layer.group_discard)(f"{project.id}_notification", self.channel_name)
            async_to_sync(self.channel_layer.group_discard)(f"{user.id}_notification", self.channel_name)

    def notifications_notification(self, event):
        self.send(text_data=event["data"])
