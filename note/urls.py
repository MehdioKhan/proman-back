from .views import NoteViewSet
from rest_framework.routers import SimpleRouter


router = SimpleRouter()

router.register('note',NoteViewSet)

urlpatterns = router.urls
